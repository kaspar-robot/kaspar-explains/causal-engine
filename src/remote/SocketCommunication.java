package remote;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Patrick Holthaus
 */
public class SocketCommunication {
      
    public static void sendExplanation (String explanation) {
        System.out.println("Sending top explanation: " + explanation);
        try {
            Socket socket = new Socket("127.0.0.1", 8328);
            ObjectOutputStream objOutStream = new ObjectOutputStream(socket.getOutputStream());
            objOutStream.writeObject("{explanation: \""+ explanation + "\"}");
            objOutStream.flush();
            objOutStream.close();
        } catch (IOException ex) {
            Logger.getLogger(SocketCommunication.class.getName()).log(Level.SEVERE, "Could not send explanation: " + ex.getMessage());
        }
        System.out.println("Done!");
    }
}
