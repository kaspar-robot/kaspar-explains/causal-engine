package core;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;

import remote.SocketCommunication;

public class Main {

    static JTextField textField;

    public static void main(String[] argv) throws Exception {

        textField = new JTextField();
        textField.addKeyListener(new CausalAnalysis());
        textField.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"), "none");

//	  textField.setText(textField.getText()+"Press: /r/n"
//	  		+ "1 for wrong object;/r/n"
//			+ "2 for animal to the left of Kaspar;\r\n"
//		  + "3 for animal to the right of Kaspar;\r\n"
//		  + "4 for wrong rotation;\r\n"
//		  + "5 for object too high;\r\n"
//		  + "6 for object too low;\r\n"
//		  + "7 for animal too far away;\r\n"
//		  + "8 for animal too close;\r\n"
//		  + "s for sleeping;\r\n"
//		  + "b for blindfolded;\r\n"
//		  + "w for wall;\r\n\r\n");
//	  
        JFrame jframe = new JFrame();
        jframe.add(textField);
        jframe.setSize(400, 350);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

class CausalAnalysis extends KeyAdapter {

    @Override
    public void keyReleased(KeyEvent event) {
        
        System.out.println(KeyEvent.getKeyText(event.getKeyCode()));
        
        switch (event.getKeyCode()) {
            case KeyEvent.VK_ENTER:
                String text = Main.textField.getText();
                // re-send previous explanation if field empty
                if (text.equals("")) {
                    this.sendExplanations(causalModel.causes);
                } else {
                    causalModel.clear();
                    if (text.contains("1")) {
                        causalModel.chosenObject = "Wrong";
                    }
                    if (text.contains("2")) {
                        causalModel.chosenAnimalPosition = "Left";
                    }
                    if (text.contains("3")) {
                        causalModel.chosenAnimalPosition = "Right";
                    }
                    if (text.contains("8")) {
                        causalModel.chosenAnimalRotation = "Wrong";
                    }
                    if (text.contains("4")) {
                        causalModel.chosenAnimalHeight = "High";
                    }
                    if (text.contains("5")) {
                        causalModel.chosenAnimalHeight = "Low";
                    }
                    if (text.contains("6")) {
                        causalModel.chosenAnimalDistance = "Far";
                    }
                    if (text.contains("7")) {
                        causalModel.chosenAnimalDistance = "Close";
                    }
                    if (text.contains("]")) {
                        causalModel.kasparState = "Asleep";
                    }
                    if (text.contains("=")) {
                        causalModel.kasparEye = "Covered";
                    }
                    if (text.contains("-")) {
                        causalModel.kasparView = "Obstructed";
                    }
                    if (text.contains("9")) {
                        causalModel.kasparHeadPosition = "Left";
                    }
                    if (text.contains("0")) {
                        causalModel.kasparHeadPosition = "Right";
                    }
                    List<Entry<String, String>> causes = causalModel.determineCauses();
                    this.sendExplanations(causes);
                }   Main.textField.setText("");
                
                break;
            case KeyEvent.VK_BACK_SPACE:
                // shuffle
                if (causalModel.causes != null) {
                    this.sendExplanations(causalModel.reorderCauses());
                } else {
                    this.sendExplanations(causalModel.causes);
                }   break;
            case KeyEvent.VK_DELETE:
                // clear text field
                Main.textField.setText("");
                break;
            default:
                break;
        }

    }

    public void sendExplanations(List<Entry<String, String>> causes) {
        if (causes == null) {
            System.out.println("\r\nNo explanation generated!\r\n");
        } else if (causes.isEmpty()) {
            System.out.println("\r\nNo explanation found!\r\n");
        } else {
            System.out.println("\r\nThe explanations are: \r\n");
            for (Entry<String, String> cause : causes) {
                System.out.println("" + cause.getKey() + "Is" + cause.getValue());
            }
            System.out.println("");
            Entry<String, String> topCause = causes.get(0);
            String topExplanation = "" + topCause.getKey() + "Is" + topCause.getValue();
            SocketCommunication.sendExplanation(topExplanation);
        }
    }
}
