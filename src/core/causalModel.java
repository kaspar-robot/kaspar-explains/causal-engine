package core;

import java.lang.reflect.Field;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class causalModel {

	
	public static String kasparEye = "correct";
	public static String kasparState = "correct";
	public static String kasparView = "correct";
	
	public static String chosenObject = "correct";
        public static String kasparHeadPosition = "correct";
	public static String chosenAnimalPosition = "correct";
	public static String chosenAnimalHeight = "correct";
	public static String chosenAnimalDistance = "correct";
	public static String chosenAnimalRotation = "correct";
	
	public static List <Entry<String,String>> causes;
		
	public static String[] endogenousVariables = new String[9];
	
	public static void buildCausalModel() {	
		endogenousVariables[0] = kasparEye;
		endogenousVariables[1] = kasparState;
		endogenousVariables[2] = kasparView;
		
		endogenousVariables[3] = chosenObject;
                endogenousVariables[4] = kasparHeadPosition;
		endogenousVariables[5] = chosenAnimalPosition;
		endogenousVariables[6] = chosenAnimalHeight;
		endogenousVariables[7] = chosenAnimalDistance;
		endogenousVariables[8] = chosenAnimalRotation;
	}

	public static List <Entry<String,String>> determineCauses (){
		buildCausalModel();		
		causes = new ArrayList<>();

		Field field[] = causalModel.class.getDeclaredFields();
        for (int i = 0; i < endogenousVariables.length; i++) {
        	if (!"correct".equals(endogenousVariables[i])){
                Entry <String,String> pair = new SimpleEntry<> (field[i].getName(), endogenousVariables[i]);
                causes.add(pair);
        	}
        } 
		return causes;
	}
	
	public static List <Entry<String,String>> reorderCauses (){
		if (!causes.isEmpty()) {
			Entry <String,String> top = causes.get(0);
			causes.remove(0);
			causes.add(top);
		}
		return causes;
	}
	
	public static void clear() {
		kasparEye = "correct";
		kasparState = "correct";
		kasparView = "correct";
                kasparHeadPosition = "correct";
		chosenObject = "correct";
		chosenAnimalPosition = "correct";
		chosenAnimalHeight = "correct";
		chosenAnimalDistance = "correct";
		chosenAnimalRotation = "correct";
	}
	
}
